# Install QGIS Plugin

* Launch QGIS
* Plugins --> Manage and Install Plugins
* Select 'Install from ZIP'
* Point to the zip you downloaded from the releases page
* You may need to restart QGIS to ensure that it loads correctly

