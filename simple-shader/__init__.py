# -*- coding: utf-8 -*-

from qgis.core import Qgis, QgsMessageLog, QgsProviderConnectionException
from qgis.utils import iface


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """
    Load from file
    """
    #
    from .SimpleShader_Plugin import SimpleShaderPlugin
    return SimpleShaderPlugin()
