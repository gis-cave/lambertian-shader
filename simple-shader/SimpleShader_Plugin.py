# -*- coding: utf-8 -*-
import os
from os import path
import sys
import inspect

from qgis.core import Qgis, QgsApplication, QgsMessageLog, QgsProviderConnectionException, QgsProcessingProvider

cmd_folder = path.split(inspect.getfile(inspect.currentframe()))[0]
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)


from .SimpleShader_ShadedRelief import Simple_Lambert, Simple_Shadow


class SimpleShaderProvider(QgsProcessingProvider):
    def unload(self):
        pass

    def loadAlgorithms(self):
        """
        Loads all algorithms belonging to this provider.
        """
        self.addAlgorithm(Simple_Lambert())
        self.addAlgorithm(Simple_Shadow())

    def id(self):
        return 'SimpleShaders'

    def name(self):
        return self.tr('Simple Shader Tools for QGIS')

    def icon(self):
        """
        Should return a QIcon which is used for your provider inside
        the Processing toolbox.
        """
        return QgsProcessingProvider.icon(self)

    def longName(self):
        return self.name()


class SimpleShaderPlugin(object):
    def __init__(self):
        self.provider = None

    def initProcessing(self):
        """Init Processing provider for QGIS >= 3.8."""
        try:
            self.provider = SimpleShaderProvider()
            QgsApplication.processingRegistry().addProvider(self.provider)
            QgsMessageLog.logMessage("Successfully loaded.", 'ShaderPro', level=Qgis.Success)
        except ImportError:
            QgsMessageLog.logMessage("Unable to load plugin... ", Qgis.Critical)
            raise QgsProviderConnectionException("Cannot load Shader  plugin.  See TerrainShader tab in the log panel.")
        return True


    def initGui(self):
        self.initProcessing()

    def unload(self):
        QgsApplication.processingRegistry().removeProvider(self.provider)
