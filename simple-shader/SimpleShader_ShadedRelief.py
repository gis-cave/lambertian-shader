# -*- coding: utf-8 -*-

import os
from math import sin, cos, pi
import numpy as np
from osgeo import gdal

from qgis.core import (
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterRasterDestination,
                       QgsProcessingParameterNumber,
                       QgsProcessingException, QgsProcessingFeedback,
                       QgsRasterFileWriter
                       )
from qgis.PyQt.QtCore import QCoreApplication

class Relief_Tool_Algorithm(QgsProcessingAlgorithm):
    OUTPUT = 'OUTPUT'
    INPUT = 'INPUT'
    VE = 'VE'
    AZ = 'AZ'
    EL = 'EL'

    def createInstance(self):
        return Relief_Tool_Algorithm()

    def __init__(self, **kwargs) -> None:
        super().__init__()
        if 'toolname' in kwargs:
            _label = kwargs.get('toolname')
        else:
            _label = __class__.__name__
        self.__name__ = _label.replace('_', '').replace('Algorithm', '').lower()
        self.__displayname__ = _label.replace('_', ' ').replace(' Algorithm', '').strip()
        self.__helpstr__ = self.__doc__
        self.__group__ = "Simple Shaders"


    def initAlgorithm(self, config):
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.INPUT,
                description='Digital Elevation Model'
            ) )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.AZ,
                description="Azimuth (In degrees; clockwise from north)",
                type=QgsProcessingParameterNumber.Double,
                defaultValue=337.5,
                minValue=0,
                maxValue=360
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.EL,
                description="Elevation (In degrees above the horizon)",
                type=QgsProcessingParameterNumber.Double,
                defaultValue=45,
                minValue=0,
                maxValue=90
            )
        )
        self.addParameter(
            QgsProcessingParameterRasterDestination(
                self.OUTPUT,
                description="Output"
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
       pass

    def name(self):
        return self.__name__

    def displayName(self):
        return self.tr(self.__displayname__)

    def group(self):
        return self.tr(self.__group__)

    def groupId(self):
        return self.__group__

    def shortHelpString(self):
        return self.__helpstr__

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def terrain_from_DEM(self, fname):
        dem = gdal.Open( fname,  gdal.GA_ReadOnly)
        if dem == None:
            raise OSError("Could not load input DEM: {}".format(fname))
        srs = dem.GetSpatialRef()
        xform = dem.GetGeoTransform()
        if not srs.IsProjected():
            raise RuntimeError("Must use DEM in a projected spatial reference.")
        if dem.RasterCount != 1:
            raise RuntimeError("I expect DEM rasters made of a single band holding elevation values.")
        demData = dem.GetRasterBand(1).ReadAsArray()
        ndv = dem.GetRasterBand(1).GetNoDataValue()

        demData[demData == ndv] = 0

        dem = None
        return demData, xform, srs


    def write_terrain_layer(self, r, xform, crs, outFile):
        """
        Writes one of the xarray 'layers' to the named file

        :param outFile: Filename to write
        :param t: Terrain data structure (must be already populated)
        :param k: The key to specify which 'layer' of the terrain to export.  Example: 'SNV', 'DEM', etc.
        """
        DriverName = QgsRasterFileWriter.driverForExtension(os.path.splitext(outFile)[1])
        if not DriverName:
            DriverName = 'GTiff'
            outFile = outFile.replace('.', '_') + ".tif"
        driver = gdal.GetDriverByName(DriverName)

        x_ = r.shape[0]
        y_ = r.shape[1]
        z_ = 1

        out_ds = driver.Create(outFile, y_, x_, z_, gdal.GDT_Float32)
        if out_ds == None:
            raise OSError("Could not create outfile {}".format(outFile))
        out_ds.SetGeoTransform(xform)
        out_ds.SetProjection(crs.ExportToWkt())

        ## Single-band output
        band = out_ds.GetRasterBand(1)
        band.WriteArray(r)
        out_ds = None


class Simple_Lambert(Relief_Tool_Algorithm):
    """
    Simple Lambert Shader ; computes theoretical reflectance value based on Lambert's Cosine Emission Law.
    Output values are in the range [-1, 1] and can be used as a 'shading' layer in a GIS.
    """
    POST_PROCESS = "POST_PROCESS"
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.__helpstr__ = self.__doc__

    def initAlgorithm(self, config):
        super().initAlgorithm(config)
        return

    def processAlgorithm(self, parameters, context, feedback):
        if feedback is None:
            feedback = QgsProcessingFeedback()
        try:
            dem_path = str(self.parameterAsRasterLayer(parameters, self.INPUT, context).source())
            feedback.pushInfo("Reading {} ...".format(dem_path))
            DEM, xform, crs = self.terrain_from_DEM(dem_path)
        except RuntimeError as e:
            feedback.reportError("It is pitch black; you are likely to be eaten by a grue.\n", fatalError=True)
            raise QgsProcessingException(e)

        az = float(self.parameterAsDouble(parameters, self.AZ, context ))
        el = float(self.parameterAsDouble(parameters, self.EL, context ))

        ## Surface Normal Vectors computed from elevation data
        [dx, dy] = _gradients(DEM, xform[1])
        mag = np.sqrt(dx**2 + dy**2 + 1)
        SNV = np.stack([-dx / mag, -dy / mag, 1 / mag], 2)

        _lambert = np.dot(SNV, _lightVector(az, el))

        output_path = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        self.write_terrain_layer(_lambert, xform, crs, output_path)
        return {self.OUTPUT: output_path}

    def createInstance(self):
        return self.__class__()



class Simple_Shadow(Relief_Tool_Algorithm):
    """
    Casts shadows over the specified terrain. 1 for enshadowed pixels, NoData for unshadowed pixels.
    """
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.__helpstr__ = self.__doc__

    def processAlgorithm(self, parameters, context, feedback):
        if feedback is None:
            feedback = QgsProcessingFeedback()
        try:
            dem_path = str(self.parameterAsRasterLayer(parameters, self.INPUT, context).source())
            feedback.pushInfo("Reading {} ...".format(dem_path))
            DEM, xform, crs = self.terrain_from_DEM(dem_path)
        except RuntimeError as e:
            feedback.reportError("It is pitch black; you are likely to be eaten by a grue.\n", fatalError=True)
            raise QgsProcessingException(e)

        az = float(self.parameterAsDouble(parameters, self.AZ, context ))
        el = float(self.parameterAsDouble(parameters, self.EL, context ))

        shadowArray = _shadowLine(DEM, xform[1], az, el)
        shadowArray[shadowArray > 0] = 1
        shadowArray[shadowArray == 0] = None

        output_path = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        self.write_terrain_layer(shadowArray, xform, crs, output_path)
        return {self.OUTPUT: output_path}

    def createInstance(self):
        return self.__class__()



#####  ######  #####  ######  #####  ######  #####  ######  #####  ######  #####  ######

## Define some helpful functions....
def _lightVector(Az, El):
    Az_rad = float(Az) * (pi / 180)
    El_rad = float(El) * (pi / 180)
    L = np.array([
        sin(Az_rad) * cos(El_rad),  # X component
        cos(Az_rad) * cos(El_rad),  # Y component
        sin(El_rad)                 # Z component
    ])
    ## In theory, this vector is already unit length one.  But let's normalize anyway...
    L /= np.sqrt((L**2).sum())
    return L


def _gradients(A, cellwidth):
    k = 1                       # (how far to step from central cell to find a neighbor. )
    A = np.pad(A, k, 'edge')    # adds a border; covers edge cases.  Will be trimmed in the slicing manipulations.

    # Create slices of the height array, shifted in each direction; these views represent cells in the kernel.
    z1 = A[0:-(2*k), 0:-(2*k)]   # +-------+-------+-------+
    z2 = A[0:-(2*k), k:-k]       # |       |       |       |
    z3 = A[0:-(2*k), (2*k):]     # |  z1   |   z2  |   z3  |
                                 # |       |       |       |
    z4 = A[k:-k, 0:-(2*k)]       # +-------+-------+-------+
    z5 = A[k:-k, k:-k]           # |       |       |       |
    z6 = A[k:-k, (2*k):]         # |   z4  |   z5  |   z6  |
                                 # |       |       |       |
    z7 = A[(2*k):, 0:-(2*k)]     # +-------+-------+-------+
    z8 = A[(2*k):, k:-k]         # |       |       |       |
    z9 = A[(2*k):, (2*k):]       # |   z7  |   z8  |   z9  |
                                 # |       |       |       |
                                 # +-------+-------+-------+
    # For a given cell at 'z5', we can now compute finite differences among its neighbors by referring to the array
    # views in neighboring cells z1 to z9 .

    # partial dz/dx is dz_dx and partial dz/dy is dz_dy
    dz_dx = ((z3 + 2 * z6 + z9) - (z1 + 2 * z4 + z7)) / (8 * cellwidth * k)
    dz_dy = ((z1 + 2 * z2 + z3) - (z7 + 2 * z8 + z9)) / (8 * cellwidth * k)

    return [dz_dx, dz_dy]


def _shadowLine(d, cellwidth, az, el):
    """
    :param t: terrain xarray dataset.  Must include a dataarray named 'DEM' which is the height field.
    :param az: Azimuth of light source
    :param el: Elevation of light source
    :return: Array, same shape as the dataset, where 0 values are un-shadowed.  Non-zero cells are in shadow.
             The value is the height difference between actual DEM height and the theoretical height which would
             just peek out of shadow.
    """
    returnArray = np.zeros(d.shape)
    lightVector = _lightVector(az, el)

    # X,Y as geographic coordinates are not the same as X,Y in numpy arrays.
    # switch to row/col names for numpy manipulations to save our sanity.
    lineMax = returnArray.shape[0] - 1
    colMax = returnArray.shape[1] - 1

    delta_col = lightVector[0]
    delta_row = -lightVector[1]
    deltha_ht = lightVector[2]

    shadowLineArray = np.pad(d, 1, 'edge')

    # The counters increment or decrement based on the dominant light direction.
    # I am not fiddling with the math to get those iterables .... Using if statements to run an unrolled
    # bit of code depending on dominant lighting direction.  This is ugly, but works.

    if (az <= 45.0 or az > 315.0):
        dH_drow = abs(deltha_ht / delta_row) * cellwidth
        p = abs(delta_col / delta_row)
        for line in range(1, lineMax):
            dem = d[line - 1, :]
            n = shadowLineArray[line - 1, 1:-1]
            sl = shadowLineArray[line, 1:-1]
            a = returnArray[line - 1]
            if delta_col <= 0:
                nw = shadowLineArray[line - 1, :-2]
            else:
                nw = shadowLineArray[line - 1, 2:]
            tstHeights = (n * (1 - p)) + (nw * (p)) - dH_drow
            sl[dem < tstHeights] = tstHeights[dem < tstHeights]
            a[dem < tstHeights] = tstHeights[dem < tstHeights] - dem[dem < tstHeights]

    if (225 < az <= 315):
        dH_dcol = abs(deltha_ht / delta_col) * cellwidth
        p = abs(delta_row / delta_col)
        for col in range(1, colMax):
            dem = d[:, col - 1]
            w = shadowLineArray[1:-1, col - 1]
            sl = shadowLineArray[1:-1, col]
            a = returnArray[:, col - 1]
            if delta_row <= 0:
                nw = shadowLineArray[:-2, col - 1]
            else:
                nw = shadowLineArray[2:, col - 1]
            tstHeights = (w * (1 - p)) + (nw * (p)) - dH_dcol
            sl[dem < tstHeights] = tstHeights[dem < tstHeights]
            a[dem < tstHeights] = tstHeights[dem < tstHeights] - dem[dem < tstHeights]

    if (135 < az <= 225):
        dH_drow = abs(deltha_ht / delta_row) * cellwidth
        p = abs(delta_col / delta_row)
        for line in range(lineMax, 0, -1):
            dem = d[line, :]
            s = shadowLineArray[line + 2, 1:-1]
            sl = shadowLineArray[line + 1, 1:-1]
            a = returnArray[line]
            if delta_col <= 0:
                sw = shadowLineArray[line + 2, :-2]
            else:
                sw = shadowLineArray[line + 2, 2:]
            tstHeights = (s * (1 - p)) + (sw * (p)) - dH_drow
            sl[dem < tstHeights] = tstHeights[dem < tstHeights]
            a[dem < tstHeights] = tstHeights[dem < tstHeights] - dem[dem < tstHeights]

    if (45 < az <= 135):
        dH_dcol = abs(deltha_ht / delta_col) * cellwidth
        p = abs(delta_row / delta_col)
        for col in range(colMax, 0, -1):
            dem = d[:, col]
            e = shadowLineArray[1:-1, col + 2]
            sl = shadowLineArray[1:-1, col + 1]
            a = returnArray[:, col - 1]
            if delta_row <= 0:
                ne = shadowLineArray[:-2, col + 2]
            else:
                ne = shadowLineArray[2:, col + 2]
            tstHeights = (e * (1 - p)) + (ne * (p)) - dH_dcol
            sl[dem < tstHeights] = tstHeights[dem < tstHeights]
            a[dem < tstHeights] = tstHeights[dem < tstHeights] - dem[dem < tstHeights]

    return returnArray