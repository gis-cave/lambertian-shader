# -*- coding: utf-8 -*-
# License: GNU GPL v3 -- See 'LICENCE' file.

# import only the basics for now... will import more later after we get logging set up.
import logging
from logging.handlers import RotatingFileHandler
from os import path

import arcpy

# GLOBALS:
# Enabling DEBUG will alter logging behavior.
#   -> If false, only sends messages to arcpy.
#   -> If True, messages to a logfile also.
DEBUG = False

class ArcPyPassThru(logging.Handler):
    """
    Python Logging module handler to pass messages to the arcpy message queues.
    With this handler attached to a logger, you needn't call arcpy.AddMessage()
    separately -- nor should you if this is in use.
    """
    def emit(self, record):
        msg = self.format(record)
        if record.levelno >= logging.ERROR:
            arcpy.AddError(msg)
            return
        if record.levelno >= logging.WARNING:
            arcpy.AddWarning(msg)
            return
        if record.levelno >= logging.INFO:
            arcpy.AddMessage(msg)

# These need to be globals, to be used within classes, below.
arcpyHandler = ArcPyPassThru()
arcpyHandler.setFormatter( logging.Formatter("%(asctime)s >> %(message)s", datefmt='%H:%M:%S'))
logfileHandler = RotatingFileHandler(
    path.join( path.dirname(__file__), path.splitext(path.basename(__file__))[0] + ".log") ,
    maxBytes=1024*1024*2,  # 2MB log files
    backupCount=2
)
logfileHandler.setFormatter(logging.Formatter("%(levelname)-8s %(name)s.%(funcName)s %(asctime)s.%(msecs)02d %(message)s", datefmt='%H:%M:%S'))

globalLogger = logging.getLogger() #root logger
if not globalLogger.handlers:
    globalLogger.addHandler(arcpyHandler)
    if DEBUG:
        globalLogger.addHandler(logfileHandler)
        globalLogger.setLevel(logging.DEBUG)
    else:
        globalLogger.setLevel(logging.ERROR)


# OK, then.... ready to start doing stuff. . .
globalLogger.debug("Toolbox Loaded -- %s ", path.basename(__file__))
# We've deferred most of the imports until now, waiting for logging to get
# plumbed.  With DEBUG=True, you can help diagnose import problems, as those
# will not be reported to the logfile.
try:
    import numpy as np
    from math import pi, cos, sin
except:
    globalLogger.error("Could not import required modules.", exc_info=True)
    raise


#  > > > > > > > > > > > > > > > TOOLBOX CONTENTS < < < < < < < < < < < < < < < <
# ==========================================================================================================
class Toolbox(object):
    """
    Toolbox implementing terrain shading methods.  Emphasizes stored surface normal vectors over height field to
    drive most of the algorithms.
    """
    def __init__(self):
        self.label = "CP Simple Shaders"
        self.description = self.__doc__
        self.alias = ''.join(filter(str.isalpha, path.splitext(path.basename(__file__))[0]))
        self.tools = [  # These functions defined below...
            # Primitives
            Simple_Lambert,
            Simple_Shadow

        ]


#  > > > > > > > > > > > > > > > TOOLs  < < < < < < < < < < < < < < < <
# =============================================================================
class ReliefTool(object):
    """
    This class does not implement anything. It only exists to sub-class ....
    this boilerplate tool class handles a lot of the code which would otherwise
    be replicated in every other tool below.  Note that this tool is not listed
    in the Toolbox.tools[] list, so it will not be seen by the ArcGIS user.
    """
    _suffix = "_xxx" #<-- placeholder.. override in subclasses.
    # See update_parameters() to see how this is used.

    def __init__(self, **kwargs):
        fname = path.splitext(path.basename(__file__))[0]
        if 'toolname' in kwargs:
            self.label = kwargs.get('toolname').replace('_', ' ')
            self.LOG = logging.getLogger(kwargs.get('toolname'))
        else:
            self.label = __class__.__name__.replace('_', ' ')
            self.LOG = logging.getLogger(__class__.__name__)
        self.LOG.propagate = False
        if not self.LOG.handlers:
            self.LOG.addHandler(arcpyHandler)
            if DEBUG:
                self.LOG.addHandler(logfileHandler)
                self.LOG.setLevel(logging.DEBUG)
            else:
                self.LOG.setLevel(logging.INFO)
        # call self.LOG.<LEVEL>(message) and have the message go to the right
        # place.  Use self.LOG instead of the global logger so that messages
        # get stamped with the class name.

        self.description = self.__doc__
        #    ^^^^ We set this here to ensure that it has a value. It should be
        #         overridden in the subclass to be that class's doc string.

    def getParameterInfo(self):
        """
        These four parameters are fairly standard for any relief shading tool
        in this toolbox. The subclassed tools can call super().getParameterInfo()
        (i.e. this method) to get the boilerplate parameter list, then adjust.
        Note the order, as it will be important later.  If you are going to ask
        for different parameters than just these four, the first line of the
        subclassed tool should be:
          [inputDEM, lightAzimuth, lightElev, outputRaster] = super().getParameterInfo()
        """
        inputDEM = arcpy.Parameter(
            displayName="Elevation Raster",
            name='input',
            datatype=["GPRasterLayer"],
            parameterType="Required",
            direction="Input"
        )
        lightAzimuth = arcpy.Parameter(
            displayName="Light Azimuth",
            name="lightAz",
            datatype="GPDouble",
            parameterType="Required",
            direction="Input"
        )
        lightAzimuth.value = 337.5
        # Biland, Julien & Coltekin, Arzu. (2016). DOI:10.1080/15230406.2016.1185647

        lightElev = arcpy.Parameter(
            displayName="Light Elevation",
            name="lightEl",
            datatype="GPDouble",
            parameterType="Required",
            direction="Output"
        )
        lightElev.value = 45

        outputRaster = arcpy.Parameter(
            displayName="Output",
            name="output",
            datatype="DERasterDataset",
            parameterType="Required",
            direction="Output"
        )
        return [inputDEM, lightAzimuth, lightElev, outputRaster]

    def isLicensed(self):
        return True

    def updateParameters(self, parameters):
        # ASSUMES that parameter with index=0 is the input DEM
        if parameters[0].valueAsText:
            # the index of the output parameter differs by tool... we're
            # looking for an output param with key 'output'.  The list should be short,
            # so a linear search should be fine.
            for i in range(0, len(parameters)):
                if parameters[i].name == 'output':
                    break
            if not parameters[i].altered:
                # Sets a useful default for the output path/file.. the _suffix string is put at
                # the end of the input path name as a first guess.  User can always override.
                parameters[i].value = path.join(
                    arcpy.env.workspace,
                    path.splitext(path.basename(parameters[0].valueAsText))[0] + self._suffix
                )
        return

    def updateMessages(self, parameters):
        return

    def execute(self, parameters, messages):
        if DEBUG:
            arcpy.AddMessage("DEBUG logfile available in {}".format(logfileHandler.baseFilename))
        return

    def args(self, params):
        """
        Takes the parameter list and makes a dict out of it.
        """
        # This could, in theory, be done as a single line using a list comprehension expression
        # instead of calling this function. I chose to do it 'longhand' this way so that I can
        # insert logging calls to report the parameters to the debugging logger.
        argv = {}
        for p in params:
            self.LOG.debug("PARAM {} = {}".format(p.name, p.valueAsText))
            argv[p.name] = p  # convenience dict
        return argv

    def DEM(self, inputParam):
        inputDEM = inputParam.valueAsText
        self.statusUpdate("Reading DEM: {}".format(path.basename(inputDEM)))
        self._demInfo = arcpy.da.Describe(inputParam.value)
        arcpy.env.outputCoordinateSystem = self._demInfo['spatialReference']
        heightField = arcpy.RasterToNumPyArray(inputParam.valueAsText, nodata_to_value=0).astype(np.float64)
        return heightField

    def SNV_from_DEM(self, heightField):
        self.statusUpdate("Computing surface normal vectors...")
        ## Surface Normal Vectors computed from elevation data
        [dx, dy] = _gradients(heightField, self._demInfo['meanCellWidth'])
        mag = np.sqrt(dx**2 + dy**2 + 1)
        return np.stack([-dx / mag, -dy / mag, 1 / mag], 2)


    def write_output_raster(self, outRaster, outputParam):
        try:
            self.statusUpdate("Writing Output to {} ...".format(
                path.basename(outputParam.valueAsText)
            ))
            outRaster = arcpy.NumPyArrayToRaster(outRaster,
                arcpy.Point(
                    self._demInfo['extent'].XMin, self._demInfo['extent'].YMin
                ),
                self._demInfo['meanCellWidth'],
                self._demInfo['meanCellHeight']
            )
            outRaster.save(outputParam.valueAsText)
            self.LOG.debug(r"Successfully written to {outputParam.valueAsText}")
        except:
            self.LOG.exception("Unable to save output.",  exc_info=True)
            raise
        return


    def statusUpdate(self, msg):
        """
        Convenience function to bundle messaging -- sets the 'progressor' message in the GUI
        and also invokes the logger attached to this class.

        :param msg: Pre-formatted message to publish.
        """
        arcpy.SetProgressor("default", msg)
        self.LOG.info(msg)
        return



class Simple_Lambert(ReliefTool):
    """
    Produces shaded output per Lambert's Cosine Emission Law.  Output range
    can be manipulated according to the eventual use of the shades.
    """
    _suffix = "_HS"
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.description = self.__doc__

    def execute(self, parameters, messages):
        super().execute(parameters, messages)
        argv = self.args(parameters)
        SNV = self.SNV_from_DEM(
            self.DEM(argv['input'])
            )

        self.statusUpdate("Calculating Surface Shade Values...")
        L = _lightVector(argv['lightAz'].value, argv['lightEl'].value)

        hs = np.dot(SNV, L)

        self.write_output_raster(hs, argv['output'])
        return


class Simple_Shadow(ReliefTool):
    """
    Casts a simple shadow using Ware's 'shadow line' method.

    """
    _suffix = "_Shadow"
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.description = self.__doc__

    def execute(self, parameters, messages):
        super().execute(parameters, messages)
        argv = self.args(parameters)
        dem = self.DEM(argv["input"])
        self.statusUpdate("Calculating Shadows...")
        hs = shadowLine(
            dem,
            self._demInfo['meanCellWidth'],
            argv["lightAz"].value ,
            argv["lightEl"].value
            )
        hs[hs > 0] = 1
        ## So the output raster will only contain data for shadowed areas.
        hs[hs == 0] = np.nan
        self.write_output_raster(hs, argv['output'])
        return


## Define some helpful functions....
def _lightVector(Az, El):
    Az_rad = float(Az) * (pi / 180)
    El_rad = float(El) * (pi / 180)
    L = np.array([
        sin(Az_rad) * cos(El_rad),  # X component
        cos(Az_rad) * cos(El_rad),  # Y component
        sin(El_rad)                 # Z component
    ])
    ## In theory, this vector is already unit length one.  But let's normalize anyway...
    L /= np.sqrt((L**2).sum())
    return L


def _gradients(A, cellwidth):
    k = 1                       # (how far to step from central cell to find a neighbor. )
    A = np.pad(A, k, 'edge')    # adds a border; covers edge cases.  Will be trimmed in the slicing manipulations.

    # Create slices of the height array, shifted in each direction; these views represent cells in the kernel.
    z1 = A[0:-(2*k), 0:-(2*k)]   # +-------+-------+-------+
    z2 = A[0:-(2*k), k:-k]       # |       |       |       |
    z3 = A[0:-(2*k), (2*k):]     # |  z1   |   z2  |   z3  |
                                 # |       |       |       |
    z4 = A[k:-k, 0:-(2*k)]       # +-------+-------+-------+
    z5 = A[k:-k, k:-k]           # |       |       |       |
    z6 = A[k:-k, (2*k):]         # |   z4  |   z5  |   z6  |
                                 # |       |       |       |
    z7 = A[(2*k):, 0:-(2*k)]     # +-------+-------+-------+
    z8 = A[(2*k):, k:-k]         # |       |       |       |
    z9 = A[(2*k):, (2*k):]       # |   z7  |   z8  |   z9  |
                                 # |       |       |       |
                                 # +-------+-------+-------+
    # For a given cell at 'z5', we can now compute finite differences among its neighbors by referring to the array
    # views in neighboring cells z1 to z9 .

    # partial dz/dx is dz_dx and partial dz/dy is dz_dy
    dz_dx = ((z3 + 2 * z6 + z9) - (z1 + 2 * z4 + z7)) / (8 * cellwidth * k)
    dz_dy = ((z1 + 2 * z2 + z3) - (z7 + 2 * z8 + z9)) / (8 * cellwidth * k)

    return [dz_dx, dz_dy]


def shadowLine(d, cellwidth, az, el):
    """
    :param t: terrain xarray dataset.  Must include a dataarray named 'DEM' which is the height field.
    :param az: Azimuth of light source
    :param el: Elevation of light source
    :return: Array, same shape as the dataset, where 0 values are un-shadowed.  Non-zero cells are in shadow.
             The value is the height difference between actual DEM height and the theoretical height which would
             just peek out of shadow.
    """
    returnArray = np.zeros(d.shape)
    lightVector = _lightVector(az, el)

    # X,Y as geographic coordinates are not the same as X,Y in numpy arrays.
    # switch to row/col names for numpy manipulations to save our sanity.
    lineMax = returnArray.shape[0] - 1
    colMax = returnArray.shape[1] - 1

    delta_col = lightVector[0]
    delta_row = -lightVector[1]
    deltha_ht = lightVector[2]

    shadowLineArray = np.pad(d, 1, 'edge')

    # The counters increment or decrement based on the dominant light direction.
    # I am not fiddling with the math to get those iterables .... Using if statements to run an unrolled
    # bit of code depending on dominant lighting direction.  This is ugly, but works.

    if (az <= 45.0 or az > 315.0):
        dH_drow = abs(deltha_ht / delta_row) * cellwidth
        p = abs(delta_col / delta_row)
        for line in range(1, lineMax):
            dem = d[line - 1, :]
            n = shadowLineArray[line - 1, 1:-1]
            sl = shadowLineArray[line, 1:-1]
            a = returnArray[line - 1]
            if delta_col <= 0:
                nw = shadowLineArray[line - 1, :-2]
            else:
                nw = shadowLineArray[line - 1, 2:]
            tstHeights = (n * (1 - p)) + (nw * (p)) - dH_drow
            sl[dem < tstHeights] = tstHeights[dem < tstHeights]
            a[dem < tstHeights] = tstHeights[dem < tstHeights] - dem[dem < tstHeights]

    if (225 < az <= 315):
        dH_dcol = abs(deltha_ht / delta_col) * cellwidth
        p = abs(delta_row / delta_col)
        for col in range(1, colMax):
            dem = d[:, col - 1]
            w = shadowLineArray[1:-1, col - 1]
            sl = shadowLineArray[1:-1, col]
            a = returnArray[:, col - 1]
            if delta_row <= 0:
                nw = shadowLineArray[:-2, col - 1]
            else:
                nw = shadowLineArray[2:, col - 1]
            tstHeights = (w * (1 - p)) + (nw * (p)) - dH_dcol
            sl[dem < tstHeights] = tstHeights[dem < tstHeights]
            a[dem < tstHeights] = tstHeights[dem < tstHeights] - dem[dem < tstHeights]

    if (135 < az <= 225):
        dH_drow = abs(deltha_ht / delta_row) * cellwidth
        p = abs(delta_col / delta_row)
        for line in range(lineMax, 0, -1):
            dem = d[line, :]
            s = shadowLineArray[line + 2, 1:-1]
            sl = shadowLineArray[line + 1, 1:-1]
            a = returnArray[line]
            if delta_col <= 0:
                sw = shadowLineArray[line + 2, :-2]
            else:
                sw = shadowLineArray[line + 2, 2:]
            tstHeights = (s * (1 - p)) + (sw * (p)) - dH_drow
            sl[dem < tstHeights] = tstHeights[dem < tstHeights]
            a[dem < tstHeights] = tstHeights[dem < tstHeights] - dem[dem < tstHeights]

    if (45 < az <= 135):
        dH_dcol = abs(deltha_ht / delta_col) * cellwidth
        p = abs(delta_row / delta_col)
        for col in range(colMax, 0, -1):
            dem = d[:, col]
            e = shadowLineArray[1:-1, col + 2]
            sl = shadowLineArray[1:-1, col + 1]
            a = returnArray[:, col - 1]
            if delta_row <= 0:
                ne = shadowLineArray[:-2, col + 2]
            else:
                ne = shadowLineArray[2:, col + 2]
            tstHeights = (e * (1 - p)) + (ne * (p)) - dH_dcol
            sl[dem < tstHeights] = tstHeights[dem < tstHeights]
            a[dem < tstHeights] = tstHeights[dem < tstHeights] - dem[dem < tstHeights]

    return returnArray