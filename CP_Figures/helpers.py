
## Define some helpful functions....
from math import sin, cos, pi
import numpy as np
import csv


def _lightVector(Az, El):
    Az_rad = float(Az) * (pi / 180)
    El_rad = float(El) * (pi / 180)
    L = np.array([
        sin(Az_rad) * cos(El_rad),  # X component
        cos(Az_rad) * cos(El_rad),  # Y component
        sin(El_rad)                 # Z component
    ])
    ## In theory, this vector is already unit length one.  But let's normalize anyway...
    L /= np.sqrt((L**2).sum())
    return L


def _gradients(A, cellwidth):
    k = 1                       # (how far to step from central cell to find a neighbor. )
    A = np.pad(A, k, 'edge')    # adds a border; covers edge cases.  Will be trimmed in the slicing manipulations.

    # Create slices of the height array, shifted in each direction; these views represent cells in the kernel.
    z1 = A[0:-(2*k), 0:-(2*k)]   # +-------+-------+-------+
    z2 = A[0:-(2*k), k:-k]       # |       |       |       |
    z3 = A[0:-(2*k), (2*k):]     # |  z1   |   z2  |   z3  |
                                 # |       |       |       |
    z4 = A[k:-k, 0:-(2*k)]       # +-------+-------+-------+
    z5 = A[k:-k, k:-k]           # |       |       |       |
    z6 = A[k:-k, (2*k):]         # |   z4  |   z5  |   z6  |
                                 # |       |       |       |
    z7 = A[(2*k):, 0:-(2*k)]     # +-------+-------+-------+
    z8 = A[(2*k):, k:-k]         # |       |       |       |
    z9 = A[(2*k):, (2*k):]       # |   z7  |   z8  |   z9  |
                                 # |       |       |       |
                                 # +-------+-------+-------+
    # For a given cell at 'z5', we can now compute finite differences among its neighbors by referring to the array
    # views in neighboring cells z1 to z9 .

    # partial dz/dx is dz_dx and partial dz/dy is dz_dy
    dz_dx = ((z3 + 2 * z6 + z9) - (z1 + 2 * z4 + z7)) / (8 * cellwidth * k)
    dz_dy = ((z1 + 2 * z2 + z3) - (z7 + 2 * z8 + z9)) / (8 * cellwidth * k)

    return [dz_dx, dz_dy]






def _shadowLine(d, dx, az, el):
    """
    :param t: terrain xarray dataset.  Must include a dataarray named 'DEM' which is the height field.
    :param az: Azimuth of light source
    :param el: Elevation of light source
    :return: Array, same shape as the dataset, where 0 values are un-shadowed.  Non-zero cells are in shadow.
             The value is the height difference between actual DEM height and the theoretical height which would
             just peek out of shadow.
    """
    def _lightVector(Az, El):
        Az_rad = float(Az) * (np.pi / 180)
        El_rad = float(El) * (np.pi / 180)
        L = np.array([
            np.sin(Az_rad) * np.cos(El_rad),  # X
            np.cos(Az_rad) * np.cos(El_rad),  # Y
            np.sin(El_rad)                 # Z
        ])
        ## In theory, this vector is already unit length one.
        ## But let's normalize anyway...
        L /= np.sqrt((L**2).sum())
        return L

    cellwidth = dx

    returnArray = np.zeros(d.shape)
    lightVector = _lightVector(az, el)

    # X,Y as geographic coordinates are not the same as X,Y in numpy arrays.
    # switch to row/col names for numpy manipulations to save our sanity.
    lineMax = returnArray.shape[0] - 1
    colMax = returnArray.shape[1] - 1

    delta_col = lightVector[0]
    delta_row = -lightVector[1]
    deltha_ht = lightVector[2]

    shadowLineArray = np.pad(d, 1, 'edge')

    # The counters increment or decrement based on the dominant light direction.
    # I am not fiddling with the math to get those iterables .... Using if statements to run an unrolled
    # bit of code depending on dominant lighting direction.  This is ugly, but works.

    if (az <= 45.0 or az > 315.0):
        dH_drow = abs(deltha_ht / delta_row) * cellwidth
        p = abs(delta_col / delta_row)
        for line in range(1, lineMax):
            dem = d[line - 1, :]
            n = shadowLineArray[line - 1, 1:-1]
            sl = shadowLineArray[line, 1:-1]
            a = returnArray[line - 1]
            if delta_col <= 0:
                nw = shadowLineArray[line - 1, :-2]
            else:
                nw = shadowLineArray[line - 1, 2:]
            tstHeights = (n * (1 - p)) + (nw * (p)) - dH_drow
            sl[dem < tstHeights] = tstHeights[dem < tstHeights]
            a[dem < tstHeights] = tstHeights[dem < tstHeights] - dem[dem < tstHeights]

    if (225 < az <= 315):
        dH_dcol = abs(deltha_ht / delta_col) * cellwidth
        p = abs(delta_row / delta_col)
        for col in range(1, colMax):
            dem = d[:, col - 1]
            w = shadowLineArray[1:-1, col - 1]
            sl = shadowLineArray[1:-1, col]
            a = returnArray[:, col - 1]
            if delta_row <= 0:
                nw = shadowLineArray[:-2, col - 1]
            else:
                nw = shadowLineArray[2:, col - 1]
            tstHeights = (w * (1 - p)) + (nw * (p)) - dH_dcol
            sl[dem < tstHeights] = tstHeights[dem < tstHeights]
            a[dem < tstHeights] = tstHeights[dem < tstHeights] - dem[dem < tstHeights]

    if (135 < az <= 225):
        dH_drow = abs(deltha_ht / delta_row) * cellwidth
        p = abs(delta_col / delta_row)
        for line in range(lineMax, 0, -1):
            dem = d[line, :]
            s = shadowLineArray[line + 2, 1:-1]
            sl = shadowLineArray[line + 1, 1:-1]
            a = returnArray[line]
            if delta_col <= 0:
                sw = shadowLineArray[line + 2, :-2]
            else:
                sw = shadowLineArray[line + 2, 2:]
            tstHeights = (s * (1 - p)) + (sw * (p)) - dH_drow
            sl[dem < tstHeights] = tstHeights[dem < tstHeights]
            a[dem < tstHeights] = tstHeights[dem < tstHeights] - dem[dem < tstHeights]

    if (45 < az <= 135):
        dH_dcol = abs(deltha_ht / delta_col) * cellwidth
        p = abs(delta_row / delta_col)
        for col in range(colMax, 0, -1):
            dem = d[:, col]
            e = shadowLineArray[1:-1, col + 2]
            sl = shadowLineArray[1:-1, col + 1]
            a = returnArray[:, col - 1]
            if delta_row <= 0:
                ne = shadowLineArray[:-2, col + 2]
            else:
                ne = shadowLineArray[2:, col + 2]
            tstHeights = (e * (1 - p)) + (ne * (p)) - dH_dcol
            sl[dem < tstHeights] = tstHeights[dem < tstHeights]
            a[dem < tstHeights] = tstHeights[dem < tstHeights] - dem[dem < tstHeights]

    return returnArray


def _lightlist(fname):
    lts = []
    with open(fname, "r") as skyConfigFile:
        csvReadFile = csv.reader(skyConfigFile)
        for line in csvReadFile:
            # brute force -- if this line does not have three comma-separated values, we skip it.
            if (len(line) == 0) or (len(line) < 3):
                continue
            if line[0].startswith("Format"):  # special case... a comment line in the header has 2 commas in it.
                continue
            lts.append(line)
    return lts


print("Helpers Loaded...")