# Lambertian Shader

## Getting started

The examples in this repo demonstrate the effects of manipulating
the output from the canonical Lambertian surface reflectance calculator.

The ArcGIS Pro toolbox is available at [this link](https://arcg.is/1mT1Wn0).
The item description page at that link includes brief instructions on how to install/use the tool.

The QGIS plugin is available on the [release page](https://gitlab.com/gis-cave/lambertian-shader/-/releases/permalink/latest). Download the zip file and install it using the QGIS plugin manager (see [the install notes](./simple-shaders/INSTALL.md))
